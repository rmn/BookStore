-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2015 年 06 月 19 日 02:49
-- 服务器版本: 5.6.12-log
-- PHP 版本: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `bookstore`
--
CREATE DATABASE IF NOT EXISTS `bookstore` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bookstore`;

-- --------------------------------------------------------

--
-- 表的结构 `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bookname` varchar(255) NOT NULL,
  `publisher` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `price` double(120,2) NOT NULL,
  `ptime` int(11) NOT NULL,
  `pic` varchar(255) NOT NULL,
  `detail` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- 转存表中的数据 `books`
--

INSERT INTO `books` (`id`, `bookname`, `publisher`, `author`, `price`, `ptime`, `pic`, `detail`) VALUES
(16, 'qq', 'qq', 'qq', 12.00, 1428931214, '20150413132014_948.jpg', ''),
(17, 'ww', 'ww', 'ww', 23.00, 1428931225, '20150413132025_979.jpg', ''),
(18, 'ee', 'ee', 'ee', 23.00, 1428931236, '20150413132036_834.jpg', ''),
(19, 'rr', 'rr', 'rr', 0.00, 1428931252, '20150413132052_199.jpg', ''),
(20, 'tt', 'tt', 'tt', 55.00, 1428931268, '20150413132108_794.jpg', ''),
(21, 'yy', 'yy', 'yy', 56.00, 1428931284, '20150413132124_635.jpg', ''),
(22, 'uu', 'uu', 'uu', 67.00, 1428931298, '20150413132138_320.jpg', ''),
(23, 'ii', 'ii', 'ii', 88.00, 1428931311, '20150413132150_723.jpg', ''),
(24, 'oo', 'oo', 'oo', 78.00, 1428931322, '20150413132202_786.jpg', ''),
(25, 'pp', 'pp', 'pp', 88.00, 1428931335, '20150413132215_895.jpg', ''),
(26, 'wqeqw', 'wqe', 'qwe', 0.00, 1428931346, '20150413132226_725.jpg', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
