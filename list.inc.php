<?php
    $ser = !empty($_POST)?$_POST:$_GET;
    $where = array();
    $param = "";
    $title = "";
    if(!empty($ser["bookname"])){
        $where[] = "bookname like '%{$ser["bookname"]}%'";
        $param .= "&bookname={$ser["bookname"]}";
        $title .= ' 图书名称中包含"'.$ser["bookname"].'"的</h3></div>';
    }
    if(!empty($ser["publisher"])){
        $where[] = "publisher like '%{$ser["publisher"]}%'";
        $param .= "&publisher={$ser["publisher"]}";
        $title .= ' 出版社名称中包含"'.$ser["publisher"].'"的';
    }
    if(!empty($ser["author"])){
        $where[] = "author like '%{$ser["author"]}%'";
        $param .= "&author={$ser["author"]}";
        $title .= ' 图书作者名字中包含"'.$ser["author"].'"的';
    }
    if(!empty($ser["startprice"])){
        $where[] = "price > '{$ser["startprice"]}'";
        $param .= "&startprice={$ser["startprice"]}";
        $title .= ' 图书价格￥大于"'.$ser["startprice"].'"的';
    }
    if(!empty($ser["endprice"])){
        $where[] = "price <= '{$ser["endprice"]}'";
        $param .= "&endprice={$ser["endprice"]}";
        $title .= ' 图书价格￥小于"'.$ser["endprice"].'"的';
    }

    if(!empty($where)){
        $where = "where ".implode(" and ", $where);
        $title = "搜索：".$title;
    }else{
        $where = "";
        $title = "图书列表：";
    }
    echo '<h3>'.$title.'</h3>';
?>

<table>
    <tr align="left" bgcolor="#cccccc">
        <th>ID</th>
        <th>图书封面</th>
        <th>图书名称</th>
        <th>出版商</th>
        <th>图书作者</th>
        <th>图书价格</th>
        <th>上架时间</th>
        <th>操作</th>
    </tr>
<?php
    include 'conn.inc.php';
    include 'page.class.php';
    $sql = "select count(*) from books {$where}";
    $result = mysql_query($sql);
    list($total) = mysql_fetch_row($result);
    $page = new Page($total,10,$param,true);
    //$page = new Page($total);
    $sql = "select id,bookname,publisher,author,price,pic,ptime from books {$where} order by id desc {$page->limit}";
    $result = mysql_query($sql);
    if($result && mysql_num_rows($result) > 0){
        $i = 0;
        while(list($id,$bookname,$publisher,$author,$price,$pic,$ptime) = mysql_fetch_row(
        $result)){
            if($i++%2==0){
                echo '<tr bgcolor="#eeeeee">';
            }
            else{
                echo '<tr>';
            }
            echo '<td>'.$id.'</td>';
            echo '<td><img width=100px src="./uploads/'.$pic.'" alt='.$bookname.'></td>';
            echo '<td>'.$bookname.'</td>';
            echo '<td>'.$publisher.'</td>';
            echo '<td>'.$author.'</td>';
            echo '<td>￥'.number_format($price,2,'.',' ').'</td>';
            echo '<td>'.date('Y-m-d',$ptime).'</td>';
            echo '<td><a href="index.php?action=mod&id='.$id.'">修改</a>/<a onclick="return confirm(\'你确定要删除食品'.$bookname.'吗？\')" href="index.php?action=del&id='.$id.'&pic='.$pic.'">删除</a></td>';
            echo '</tr>';
        }
        echo '<tr><td colspan="7"align="right">'.$page->fpage().'</td></tr>';
    }else{
        echo '<tr><td colspan="7"align="center">没有找到相关食品</td></tr>';
    }
    mysql_free_result($result);
    mysql_close($link);
?>
</table>

