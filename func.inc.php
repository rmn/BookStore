<?php
    include 'fileupload.class.php';
    include 'image.class.php';
    /*处理图片上传*/
    function upload(){
        $path = "./uploads/";
        $up = new FileUpload($path);
        if($up -> upload('pic')){
            $filename = $up->getFileName();
            $img = new Image($path);
            $img -> thumb($filename,300,300,"");
            $img -> thumb($filename,80,80,"icon_");
            $img -> thumb($filename,40,40,"icon1_");
            //$img -> waterMark($filename,"./images/logo.gif",5,"");
            return array(true,$filename);
        }else{
            return array(false,$up -> getErrorMsg());
        }
    }
    /*删除上传的图片*/
    function delpic($picname){
        $path = "./uploads/";
        @unlink($path.$picname);
        @unlink($path.'icon_'.$picname);
        @unlink($path.'icon1_'.$picname);
    }   




?>
